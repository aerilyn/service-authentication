package healthcheck

import (
	"gitlab.com/aerilyn/service-authentication/internal/app/healthcheck/delivery/web"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	web.NewHandlerRegistry,
)
