package app

import (
	authentication "gitlab.com/aerilyn/service-authentication/internal/app/authentication/delivery/web"
	healthcheckweb "gitlab.com/aerilyn/service-authentication/internal/app/healthcheck/delivery/web"
)

// jhipster-needle-import-handler

type RequiredHandlers struct {
	AuthenticationHTTPHandlerRegistry *authentication.AuthenticationHandlerRegistry
	HealthCheckHTTPHandlerRegistry    *healthcheckweb.HealthCheckHandlerRegistry
}
