package repositorymongo

import (
	"context"
	"fmt"

	"gitlab.com/aerilyn/service-authentication/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

type MongoUserRepositoryOptions struct {
	Db *mongo.Database
}

type MongoUserRepository struct {
	options MongoUserRepositoryOptions
}

func NewMongoUserRepository(options MongoUserRepositoryOptions) *MongoUserRepository {
	return &MongoUserRepository{options: options}
}

func (m *MongoUserRepository) collection() *mongo.Collection {
	return m.options.Db.Collection(CollectionUser)
}

func (m *MongoUserRepository) Create(ctx context.Context, cmd ent.User) (*string, errors.CodedError) {
	result, err := m.collection().InsertOne(ctx, cmd)
	if err != nil {
		return nil, errors.NewInternalSystemError()
	}
	id := fmt.Sprintf("%v", result.InsertedID.(primitive.ObjectID).Hex())
	return &id, nil
}

func (m *MongoUserRepository) FindByUsername(ctx context.Context, username string) (*ent.User, errors.CodedError) {
	filter := bson.M{"username": username}
	user := ent.User{}
	findErr := m.collection().FindOne(ctx, filter).Decode(&user)
	if findErr == mongo.ErrNoDocuments {
		return nil, errors.NewEntityNotFound()
	}
	if findErr != nil {
		log.WithCTX(ctx).Error(findErr)
		return nil, errors.NewInternalSystemError()
	}
	return &user, nil
}

func (m *MongoUserRepository) FindByUsernamePassword(ctx context.Context, username, password string) (*ent.User, errors.CodedError) {
	filter := bson.M{"username": username, "password": password}
	user := ent.User{}
	findErr := m.collection().FindOne(ctx, filter).Decode(&user)
	if findErr == mongo.ErrNoDocuments {
		return nil, errors.NewEntityNotFound()
	}
	if findErr != nil {
		return nil, errors.NewInternalSystemError()
	}
	return &user, nil
}

func (m *MongoUserRepository) Find(ctx context.Context, id string) (*ent.User, errors.CodedError) {
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	filter := bson.M{"_id": objectId}
	user := ent.User{}
	findErr := m.collection().FindOne(ctx, filter).Decode(&user)
	if findErr == mongo.ErrNoDocuments {
		return nil, errors.NewEntityNotFound()
	}
	if findErr != nil {
		return nil, errors.NewInternalSystemError()
	}
	return &user, nil
}
