package repository

//go:generate mockgen -source=user.go -destination=repositorymock/user_mock.go -package=repositorymock

import (
	"context"

	"gitlab.com/aerilyn/service-authentication/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"

	"go.mongodb.org/mongo-driver/bson"
)

type FindByUserIDOptions struct {
	Limit  int64
	Offset int64
	Filter bson.M
}

type UserRepository interface {
	Create(ctx context.Context, cmd ent.User) (*string, errors.CodedError)
	Find(ctx context.Context, id string) (*ent.User, errors.CodedError)
	FindByUsername(ctx context.Context, username string) (*ent.User, errors.CodedError)
	FindByUsernamePassword(ctx context.Context, username, passowrd string) (*ent.User, errors.CodedError)
}
