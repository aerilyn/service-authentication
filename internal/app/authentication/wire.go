package authentication

import (
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/delivery/web"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/repository"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/repository/repositorymongo"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase/usecaseimpl"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase/validatorimpl"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet(
	wire.Struct(new(repositorymongo.MongoUserRepositoryOptions), "*"),
	repositorymongo.NewMongoUserRepository,
	wire.Bind(new(repository.UserRepository), new(*repositorymongo.MongoUserRepository)),
)

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.AuthenticationRegistryOptions), "*"),
	web.NewAuthenticationHandlerRegistry,
)

var validatorModuleSet = wire.NewSet(
	wire.Struct(new(validatorimpl.SignInValidatorOptions), "*"),
	validatorimpl.NewSignInValidator,
	wire.Bind(new(usecase.SignInValidator), new(*validatorimpl.SignInValidator)),

	wire.Struct(new(validatorimpl.SignUpValidatorOptions), "*"),
	validatorimpl.NewSignUpValidator,
	wire.Bind(new(usecase.SignUpValidator), new(*validatorimpl.SignUpValidator)),
)

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.AuthenticationSignInOptions), "*"),
	usecaseimpl.NewAuthenticationSignIn,
	wire.Bind(new(usecase.AuthenticationSignIn), new(*usecaseimpl.AuthenticationSignIn)),

	wire.Struct(new(usecaseimpl.AuthenticationSignUpOptions), "*"),
	usecaseimpl.NewAuthenticationSignUp,
	wire.Bind(new(usecase.AuthenticationSignUp), new(*usecaseimpl.AuthenticationSignUp)),

	wire.Struct(new(usecaseimpl.UserByTokenOptions), "*"),
	usecaseimpl.NewUserByToken,
	wire.Bind(new(usecase.UserByToken), new(*usecaseimpl.UserByToken)),
)
