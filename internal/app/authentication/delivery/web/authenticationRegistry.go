package web

import (
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"

	"github.com/go-chi/chi"
)

type AuthenticationRegistryOptions struct {
	AuthenticationSignIn usecase.AuthenticationSignIn
	AuthenticationSignUp usecase.AuthenticationSignUp
	UserByToken          usecase.UserByToken
}

type AuthenticationHandlerRegistry struct {
	options AuthenticationRegistryOptions
}

func NewAuthenticationHandlerRegistry(options AuthenticationRegistryOptions) *AuthenticationHandlerRegistry {
	return &AuthenticationHandlerRegistry{
		options: options,
	}
}

func (h *AuthenticationHandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Post("/user/", AuthenticationSignUpWebHandler(h.options.AuthenticationSignUp))
	r.Get("/user/", AuthenticationGetUserByTokenWebHandler(h.options.UserByToken))
	r.Post("/signin/", AuthenticationSignInWebHandler(h.options.AuthenticationSignIn))
	return nil
}
