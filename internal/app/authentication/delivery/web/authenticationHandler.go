package web

import (
	"net/http"

	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/middleware"
	"gitlab.com/aerilyn/service-library/response"

	"github.com/go-chi/render"
)

func AuthenticationSignInWebHandler(authenticationSignIn usecase.AuthenticationSignIn) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		ctx := r.Context()
		cmd := usecase.SignInCommand{}
		if decodeErr := render.DecodeJSON(r.Body, &cmd); decodeErr != nil {
			err := errors.NewInternalSystemError().CopyWith(errors.Message(decodeErr.Error()))
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		result, err := authenticationSignIn.SignIn(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusOK, nil})
	}
}

func AuthenticationSignUpWebHandler(authenticationSignUp usecase.AuthenticationSignUp) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		cmd := usecase.SignUpCommand{}
		if decodeErr := render.DecodeJSON(r.Body, &cmd); decodeErr != nil {
			err := errors.NewInternalSystemError().CopyWith(errors.Message(decodeErr.Error()))
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		result, err := authenticationSignUp.SignUp(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, nil})
	}
}

func AuthenticationGetUserByTokenWebHandler(userByToken usecase.UserByToken) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		id := ctx.Value(middleware.UserIDKey).(string)
		result, err := userByToken.UserBytoken(ctx, id)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusOK, nil})
	}
}
