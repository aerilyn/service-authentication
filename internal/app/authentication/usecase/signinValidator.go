package usecase

//go:generate mockgen -source=signinValidator.go -destination=usecasemock/signinValidator_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type SignInValidator interface {
	Validate(ctx context.Context, cmd SignInCommand) errors.CodedError
}
