package usecaseimpl

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/repository/repositorymock"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase/usecasemock"
	"gitlab.com/aerilyn/service-authentication/internal/app/ent"
	cacheMock "gitlab.com/aerilyn/service-authentication/internal/pkg/cache/mock"
	"gitlab.com/aerilyn/service-library/config"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	crytographyMock "gitlab.com/aerilyn/service-library/cryptography/mock"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/hash/md5"
	md5Mock "gitlab.com/aerilyn/service-library/hash/md5/mock"
	"gitlab.com/aerilyn/service-library/jwt"
	jwtMock "gitlab.com/aerilyn/service-library/jwt/mock"
)

func TestAuthenticationSignin(t *testing.T) {
	ctx := context.Background()
	hasdMD5 := md5.NewHashMD5()
	signInCommand := usecase.SignInCommand{
		Username: "test1",
		Password: "test1",
	}

	user := ent.User{
		ID:       "123456789",
		Username: signInCommand.Username,
		Password: hasdMD5.Encrypt(signInCommand.Password),
		Fullname: "test1",
		IsActive: true,
		IsAdmin:  false,
	}

	dataUser := map[string]interface{}{
		userIDKey: user.ID,
	}
	byteDataUser, _ := json.Marshal(dataUser)
	encryptData := "1234-1234-1234-1235"
	data := map[string]interface{}{
		dataKey: encryptData,
	}
	duration := time.Hour * time.Duration(durationTime)
	Convey("Testing Authentication Signin", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		//init setup
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		userRepositoryMock := repositorymock.NewMockUserRepository(mockCtrl)
		signinValidatorMock := usecasemock.NewMockSignInValidator(mockCtrl)
		jwtMock := jwtMock.NewMockJwt(mockCtrl)
		cacheMock := cacheMock.NewMockCache(mockCtrl)
		crytographyMock := crytographyMock.NewMockCryptography(mockCtrl)
		md5Mock := md5Mock.NewMockHashMD5(mockCtrl)
		authenticationSignInOptions := AuthenticationSignInOptions{
			Config:          configMock,
			UserRepository:  userRepositoryMock,
			SignInValidator: signinValidatorMock,
			Cache:           cacheMock,
			Jwt:             jwtMock,
			Crypto:          crytographyMock,
			HashMD5:         md5Mock,
		}
		//setting jwt
		configMock.EXPECT().Get(config.JWT_KEY).Return("dicocbaAjaDulu")
		jwt := jwt.ProvideJwt(configMock)
		token, _ := jwt.CreateToken(data, duration)
		dto := usecase.SignInDTO{
			Token: token,
		}
		uc := NewAuthenticationSignIn(authenticationSignInOptions)
		Convey("when validation return error should return error and nil res", func() {
			signinValidatorMock.EXPECT().Validate(ctx, signInCommand).Return(errors.NewInternalSystemError())
			res, errCode := uc.SignIn(ctx, signInCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey("when FindByUsername return error should return error and nil res", func() {
			signinValidatorMock.EXPECT().Validate(ctx, signInCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signInCommand.Username).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.SignIn(ctx, signInCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey("when user.password != hashmd5 password should return error forbidden access and nil res", func() {
			signinValidatorMock.EXPECT().Validate(ctx, signInCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signInCommand.Username).Return(&user, nil)
			md5Mock.EXPECT().Encrypt(signInCommand.Password).Return("xyz")
			res, errCode := uc.SignIn(ctx, signInCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewForbiddenAccessError().Code())
		})
		Convey("when encrypt payload should return error internal and nil res", func() {
			signinValidatorMock.EXPECT().Validate(ctx, signInCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signInCommand.Username).Return(&user, nil)
			md5Mock.EXPECT().Encrypt(signInCommand.Password).Return(user.Password)
			crytographyMock.EXPECT().Encrypt(string(byteDataUser)).Return("", errors.NewInternalSystemError())
			res, errCode := uc.SignIn(ctx, signInCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey("when create jwt return error should return error internal and nil res", func() {
			signinValidatorMock.EXPECT().Validate(ctx, signInCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signInCommand.Username).Return(&user, nil)
			md5Mock.EXPECT().Encrypt(signInCommand.Password).Return(user.Password)
			crytographyMock.EXPECT().Encrypt(string(byteDataUser)).Return(encryptData, nil)
			jwtMock.EXPECT().CreateToken(data, duration).Return("", errors.NewInternalSystemError())
			res, errCode := uc.SignIn(ctx, signInCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey("when all process not return error  should return error nil and dto", func() {
			signinValidatorMock.EXPECT().Validate(ctx, signInCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signInCommand.Username).Return(&user, nil)
			md5Mock.EXPECT().Encrypt(signInCommand.Password).Return(user.Password)
			crytographyMock.EXPECT().Encrypt(string(byteDataUser)).Return(encryptData, nil)
			jwtMock.EXPECT().CreateToken(data, duration).Return(token, nil)
			res, errCode := uc.SignIn(ctx, signInCommand)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &dto)
		})
	})
}
