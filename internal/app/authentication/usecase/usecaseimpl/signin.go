package usecaseimpl

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/repository"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"
	"gitlab.com/aerilyn/service-authentication/internal/pkg/cache"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/cryptography"
	"gitlab.com/aerilyn/service-library/errors"
	pkgMD5 "gitlab.com/aerilyn/service-library/hash/md5"
	"gitlab.com/aerilyn/service-library/jwt"
)

const (
	durationTime int64  = 720
	userIDKey    string = "user_id"
	dataKey      string = "data"
)

type AuthenticationSignInOptions struct {
	Config          config.ConfigEnv
	UserRepository  repository.UserRepository
	Cache           cache.Cache
	Jwt             jwt.Jwt
	Crypto          cryptography.Cryptography
	HashMD5         pkgMD5.HashMD5
	SignInValidator usecase.SignInValidator
}

type AuthenticationSignIn struct {
	options AuthenticationSignInOptions
}

func NewAuthenticationSignIn(options AuthenticationSignInOptions) *AuthenticationSignIn {
	return &AuthenticationSignIn{
		options: options,
	}
}

func (s *AuthenticationSignIn) SignIn(ctx context.Context, cmd usecase.SignInCommand) (*usecase.SignInDTO, errors.CodedError) {
	err := s.options.SignInValidator.Validate(ctx, cmd)
	if err != nil {
		return nil, err
	}

	user, err := s.options.UserRepository.FindByUsername(ctx, cmd.Username)
	if err != nil {
		return nil, err
	}

	if user.Password != s.options.HashMD5.Encrypt(cmd.Password) {
		return nil, errors.NewForbiddenAccessError()
	}

	dataUser := map[string]interface{}{
		userIDKey: user.ID,
	}
	stringJson, _ := json.Marshal(dataUser)
	encryptData, errEncrypt := s.options.Crypto.Encrypt(string(stringJson))
	if errEncrypt != nil {
		return nil, errors.NewInternalSystemError()
	}

	data := map[string]interface{}{
		dataKey: encryptData,
	}
	duration := time.Hour * time.Duration(durationTime)
	token, errCreateToken := s.options.Jwt.CreateToken(data, duration)
	if errCreateToken != nil {
		return nil, errCreateToken
	}
	dto := usecase.SignInDTO{
		Token: token,
	}
	return &dto, nil
}
