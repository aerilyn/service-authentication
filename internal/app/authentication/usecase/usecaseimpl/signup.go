package usecaseimpl

import (
	"context"
	"encoding/json"
	"time"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill/message"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/repository"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"
	"gitlab.com/aerilyn/service-authentication/internal/app/ent"
	"gitlab.com/aerilyn/service-authentication/internal/pkg/cache"
	pkgConfig "gitlab.com/aerilyn/service-authentication/internal/pkg/config"
	"gitlab.com/aerilyn/service-authentication/internal/pkg/pubsub/rabbitmq"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	pkgMD5 "gitlab.com/aerilyn/service-library/hash/md5"
	"gitlab.com/aerilyn/service-library/log"
)

type AuthenticationSignUpOptions struct {
	Config          config.ConfigEnv
	UserRepository  repository.UserRepository
	Cache           cache.Cache
	HashMD5         pkgMD5.HashMD5
	SignUpValidator usecase.SignUpValidator
	Publisher       rabbitmq.Publisher
}

type AuthenticationSignUp struct {
	options AuthenticationSignUpOptions
}

func NewAuthenticationSignUp(options AuthenticationSignUpOptions) *AuthenticationSignUp {
	return &AuthenticationSignUp{
		options: options,
	}
}

func (s *AuthenticationSignUp) SignUp(ctx context.Context, cmd usecase.SignUpCommand) (*usecase.SignUpDTO, errors.CodedError) {
	err := s.options.SignUpValidator.Validate(ctx, cmd)
	if err != nil {
		return nil, err
	}

	user, err := s.options.UserRepository.FindByUsername(ctx, cmd.Username)
	if err != nil && err.Code() != errors.NewEntityNotFound().Code() {
		log.WithCTX(ctx).Info("error when FindByUsername")
		return nil, err
	}

	if user != nil {
		return nil, errors.NewIdempotentTransactionViolationError()
	}
	ent := ent.User{
		Username:  cmd.Username,
		Password:  s.options.HashMD5.Encrypt(cmd.Password),
		Fullname:  cmd.FullName,
		Email:     cmd.Email,
		IsActive:  true,
		IsAdmin:   false,
		UpdatedAt: time.Now(),
		CreatedAt: time.Now(),
	}
	id, err := s.options.UserRepository.Create(ctx, ent)
	if err != nil {
		log.WithCTX(ctx).Info("error when Create")
		return nil, errors.NewInternalSystemError()
	}
	payload, _ := json.Marshal(cmd)
	msg := message.NewMessage(watermill.NewUUID(), payload)
	if err := s.options.Publisher.Publish(pkgConfig.EmailSuccessRegisterHandlerTopicName, msg); err != nil {
		log.WithCTX(ctx).Info("error when Publish Data")
		return nil, errors.NewInternalSystemError()
	}
	dto := usecase.SignUpDTO{
		ID: *id,
	}
	return &dto, nil
}
