package usecaseimpl

import (
	"context"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/repository/repositorymock"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase/usecasemock"
	"gitlab.com/aerilyn/service-authentication/internal/app/ent"
	cacheMock "gitlab.com/aerilyn/service-authentication/internal/pkg/cache/mock"
	pkgConfig "gitlab.com/aerilyn/service-authentication/internal/pkg/config"
	pubsubMock "gitlab.com/aerilyn/service-authentication/internal/pkg/pubsub/rabbitmq/mock"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/hash/md5"
	md5Mock "gitlab.com/aerilyn/service-library/hash/md5/mock"
)

func TestAuthenticationSignup(t *testing.T) {
	ctx := context.Background()
	hasdMD5 := md5.NewHashMD5()
	signUpCommand := usecase.SignUpCommand{
		Username: "test1",
		Password: "test1",
		FullName: "test1",
		Email:    "test1@email.test",
	}

	user := ent.User{
		ID:        "123456789",
		Username:  signUpCommand.Username,
		Password:  hasdMD5.Encrypt(signUpCommand.Password),
		Fullname:  signUpCommand.FullName,
		Email:     signUpCommand.Email,
		IsActive:  true,
		IsAdmin:   false,
		UpdatedAt: time.Now(),
		CreatedAt: time.Now(),
	}
	id := user.ID
	signUpDTO := usecase.SignUpDTO{
		ID: user.ID,
	}
	Convey("Testing Authentication Signup", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		//init setup
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		userRepositoryMock := repositorymock.NewMockUserRepository(mockCtrl)
		signupValidatorMock := usecasemock.NewMockSignUpValidator(mockCtrl)
		cacheMock := cacheMock.NewMockCache(mockCtrl)
		md5Mock := md5Mock.NewMockHashMD5(mockCtrl)
		publisherMock := pubsubMock.NewMockPublisher(mockCtrl)
		authenticationSignUpOptions := AuthenticationSignUpOptions{
			Config:          configMock,
			UserRepository:  userRepositoryMock,
			SignUpValidator: signupValidatorMock,
			Cache:           cacheMock,
			HashMD5:         md5Mock,
			Publisher:       publisherMock,
		}
		uc := NewAuthenticationSignUp(authenticationSignUpOptions)
		Convey("when validator return error should return error and dto nil", func() {
			signupValidatorMock.EXPECT().Validate(ctx, signUpCommand).Return(errors.NewInternalSystemError())
			res, errCode := uc.SignUp(ctx, signUpCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey("when FindByUsername return error should return error and dto nil", func() {
			signupValidatorMock.EXPECT().Validate(ctx, signUpCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signUpCommand.Username).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.SignUp(ctx, signUpCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey("when FindByUsername return user should return error idempotent transaction and dto nil", func() {
			signupValidatorMock.EXPECT().Validate(ctx, signUpCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signUpCommand.Username).Return(&user, nil)
			res, errCode := uc.SignUp(ctx, signUpCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewIdempotentTransactionViolationError().Code())
		})
		Convey("when Create return error should return error internal system error and dto nil", func() {
			signupValidatorMock.EXPECT().Validate(ctx, signUpCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signUpCommand.Username).Return(nil, errors.NewEntityNotFound())
			md5Mock.EXPECT().Encrypt(signUpCommand.Password).Return(user.Password)
			userRepositoryMock.EXPECT().Create(ctx, gomock.Any()).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.SignUp(ctx, signUpCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey("when Publish to MQ return error should return error internal system error and dto nil", func() {
			signupValidatorMock.EXPECT().Validate(ctx, signUpCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signUpCommand.Username).Return(nil, errors.NewEntityNotFound())
			md5Mock.EXPECT().Encrypt(signUpCommand.Password).Return(user.Password)
			userRepositoryMock.EXPECT().Create(ctx, gomock.Any()).Return(&id, nil)
			publisherMock.EXPECT().Publish(pkgConfig.EmailSuccessRegisterHandlerTopicName, gomock.Any()).Return(errors.NewInternalSystemError())
			res, errCode := uc.SignUp(ctx, signUpCommand)
			So(errCode, ShouldNotBeNil)
			So(res, ShouldBeNil)
			So(errCode.Code(), ShouldResemble, errors.NewInternalSystemError().Code())
		})
		Convey("when all process not return error  should return error nil and dto", func() {
			signupValidatorMock.EXPECT().Validate(ctx, signUpCommand).Return(nil)
			userRepositoryMock.EXPECT().FindByUsername(ctx, signUpCommand.Username).Return(nil, errors.NewEntityNotFound())
			md5Mock.EXPECT().Encrypt(signUpCommand.Password).Return(user.Password)
			userRepositoryMock.EXPECT().Create(ctx, gomock.Any()).Return(&id, nil)
			publisherMock.EXPECT().Publish(pkgConfig.EmailSuccessRegisterHandlerTopicName, gomock.Any()).Return(nil)
			res, errCode := uc.SignUp(ctx, signUpCommand)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &signUpDTO)
		})
	})
}
