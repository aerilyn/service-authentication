package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/repository"
	"gitlab.com/aerilyn/service-authentication/internal/app/ent"
	"gitlab.com/aerilyn/service-authentication/internal/pkg/cache"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
)

type UserByTokenOptions struct {
	Config         config.ConfigEnv
	UserRepository repository.UserRepository
	Cache          cache.Cache
}

type UserByToken struct {
	options UserByTokenOptions
}

func NewUserByToken(options UserByTokenOptions) *UserByToken {
	return &UserByToken{
		options: options,
	}
}

func (s *UserByToken) UserBytoken(ctx context.Context, id string) (*ent.User, errors.CodedError) {
	user, err := s.options.UserRepository.Find(ctx, id)
	if err != nil {
		return nil, err
	}
	return user, nil
}
