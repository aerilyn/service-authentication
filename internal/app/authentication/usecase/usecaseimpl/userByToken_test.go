package usecaseimpl

import (
	"context"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/repository/repositorymock"
	"gitlab.com/aerilyn/service-authentication/internal/app/ent"
	cacheMock "gitlab.com/aerilyn/service-authentication/internal/pkg/cache/mock"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/hash/md5"
)

func TestAuthenticationUserByToken(t *testing.T) {
	ctx := context.Background()
	hasdMD5 := md5.NewHashMD5()
	password := "test1"
	user := ent.User{
		ID:        "123456789",
		Username:  "test1",
		Password:  hasdMD5.Encrypt(password),
		Fullname:  "test1",
		IsActive:  true,
		IsAdmin:   false,
		UpdatedAt: time.Now(),
		CreatedAt: time.Now(),
	}
	Convey("Testing Authentication UserByToken", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		//init setup
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		userRepositoryMock := repositorymock.NewMockUserRepository(mockCtrl)
		cacheMock := cacheMock.NewMockCache(mockCtrl)
		userByTokenOptions := UserByTokenOptions{
			Config:         configMock,
			UserRepository: userRepositoryMock,
			Cache:          cacheMock,
		}
		uc := NewUserByToken(userByTokenOptions)
		Convey("when Find return error  should return error internal system and dto nil", func() {
			userRepositoryMock.EXPECT().Find(ctx, user.ID).Return(nil, errors.NewInternalSystemError())
			res, errCode := uc.UserBytoken(ctx, user.ID)
			So(res, ShouldBeNil)
			So(errCode, ShouldNotBeNil)
			So(errCode.Code(), ShouldEqual, errors.NewInternalSystemError().Code())
		})
		Convey("when all process not return error  should return error nil and dto", func() {
			userRepositoryMock.EXPECT().Find(ctx, user.ID).Return(&user, nil)
			res, errCode := uc.UserBytoken(ctx, user.ID)
			So(errCode, ShouldBeNil)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &user)
		})
	})
}
