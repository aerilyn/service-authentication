package validatorimpl

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/validation/ozzomapper"
)

type SignInValidatorOptions struct {
}

type SignInValidator struct {
	Options SignInValidatorOptions
}

func NewSignInValidator(opts SignInValidatorOptions) *SignInValidator {
	return &SignInValidator{
		Options: opts,
	}
}

func (s SignInValidator) Validate(ctx context.Context, cmd usecase.SignInCommand) errors.CodedError {
	validationErr := validation.ValidateStruct(
		&cmd,
		validation.Field(&cmd.Username, validation.Required),
		validation.Field(&cmd.Password, validation.Required),
	)
	switch e := validationErr.(type) {
	case validation.InternalError:
		return errors.NewInternalSystemError().CopyWith(errors.Message(e.Error()))
	case validation.Errors:
		invalidParameterErr := errors.NewInvalidParameterError()
		return ozzomapper.WrapValidationError(invalidParameterErr.Code(), invalidParameterErr.MessageKey(), invalidParameterErr.Message(), e)
	}
	return nil
}
