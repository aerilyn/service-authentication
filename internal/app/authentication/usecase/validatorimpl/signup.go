package validatorimpl

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/aerilyn/service-authentication/internal/app/authentication/usecase"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/validation/ozzomapper"
)

type SignUpValidatorOptions struct {
}

type SignUpValidator struct {
	Options SignUpValidatorOptions
}

func NewSignUpValidator(opts SignUpValidatorOptions) *SignUpValidator {
	return &SignUpValidator{
		Options: opts,
	}
}

func (s SignUpValidator) Validate(ctx context.Context, cmd usecase.SignUpCommand) errors.CodedError {
	validationErr := validation.ValidateStruct(
		&cmd,
		validation.Field(&cmd.Username, validation.Required),
		validation.Field(&cmd.Password, validation.Required),
		validation.Field(&cmd.FullName, validation.Required),
		validation.Field(&cmd.Email, validation.Required),
	)
	switch e := validationErr.(type) {
	case validation.InternalError:
		return errors.NewInternalSystemError().CopyWith(errors.Message(e.Error()))
	case validation.Errors:
		invalidParameterErr := errors.NewInvalidParameterError()
		return ozzomapper.WrapValidationError(invalidParameterErr.Code(), invalidParameterErr.MessageKey(), invalidParameterErr.Message(), e)
	}
	return nil
}
