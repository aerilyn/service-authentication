package usecase

//go:generate mockgen -source=signIn.go -destination=usecasemock/signIn_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type SignInCommand struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type SignInDTO struct {
	Token string `json:"token"`
}

type AuthenticationSignIn interface {
	SignIn(ctx context.Context, cmd SignInCommand) (*SignInDTO, errors.CodedError)
}
