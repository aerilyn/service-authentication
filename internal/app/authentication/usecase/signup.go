package usecase

//go:generate mockgen -source=signup.go -destination=usecasemock/signup_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type SignUpCommand struct {
	Username string `json:"username"`
	Password string `json:"password"`
	FullName string `json:"fullname"`
	Email    string `json:"email"`
}

type SignUpDTO struct {
	ID string `json:"id"`
}

type AuthenticationSignUp interface {
	SignUp(ctx context.Context, cmd SignUpCommand) (*SignUpDTO, errors.CodedError)
}
