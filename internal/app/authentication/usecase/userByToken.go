package usecase

//go:generate mockgen -source=userByToken.go -destination=usecasemock/userByToken_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-authentication/internal/app/ent"
	"gitlab.com/aerilyn/service-library/errors"
)

type UserByToken interface {
	UserBytoken(ctx context.Context, id string) (*ent.User, errors.CodedError)
}
