package usecase

//go:generate mockgen -source=signupValidator.go -destination=usecasemock/signupValidator_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type SignUpValidator interface {
	Validate(ctx context.Context, cmd SignUpCommand) errors.CodedError
}
