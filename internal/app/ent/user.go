package ent

import (
	"time"
)

//for saving transaction ecommerce payment
type User struct {
	ID        string    `json:"id" bson:"_id,omitempty"`
	Username  string    `json:"username" bson:"username"`
	Email     string    `json:"email" bson:"email"`
	Password  string    `json:"-" bson:"password"`
	Fullname  string    `json:"fullname" bson:"fullname"`
	IsActive  bool      `json:"isActive" bson:"isActive"`
	IsDelete  bool      `json:"isDelete" bson:"isDelete"`
	IsAdmin   bool      `json:"isAdmin" bson:"isAdmin"`
	CreatedAt time.Time `json:"-" bson:"created_at"`
	UpdatedAt time.Time `json:"-" bson:"updated_at"`
}
