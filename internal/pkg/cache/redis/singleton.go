package redis

import (
	"context"
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
)

var (
	redisClient *RedisClient
	onceRedis   sync.Once
)

type RedisClient struct {
	redis *redis.Client
}

func ProvideClient(cfg *config.Config) *RedisClient {
	onceRedis.Do(func() {
		addr := fmt.Sprintf("%s:%s", cfg.Get(config.REDIS_HOST), cfg.Get(config.REDIS_PORT))
		db, _ := strconv.Atoi(cfg.Get(config.REDIS_DB))
		client := redis.NewClient(&redis.Options{
			Addr:     addr,
			DB:       db,
			Password: cfg.Get(config.REDIS_PASSWORD),
		})
		ctx := context.Background()
		_, err := client.Ping(ctx).Result()
		if err != nil {
			panic(err)
		}
		fmt.Println("connect redis to host => ", addr)
		redisClient = &RedisClient{client}
	})
	return redisClient
}

func (x *RedisClient) Get(ctx context.Context, key string) (*string, errors.CodedError) {
	val, err := x.redis.Get(ctx, key).Result()
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	return &val, nil
}

func (x *RedisClient) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) errors.CodedError {
	err := x.redis.Set(ctx, key, value, expiration).Err()
	if err != nil {
		return errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	return nil
}
