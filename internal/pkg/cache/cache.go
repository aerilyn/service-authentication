package cache

import (
	"context"
	"time"

	"gitlab.com/aerilyn/service-library/errors"
)

//go:generate mockgen -source=cache.go -destination=mock/cache_mock.go -package=mock

type Cache interface {
	Get(ctx context.Context, key string) (*string, errors.CodedError)
	Set(ctx context.Context, key string, value interface{}, expiration time.Duration) errors.CodedError
}
